> This is (so far) not a secure system.
> Don't expose the service to untrusted environments.

This `bootstrap` branch will later be squashed into `master` and deleted afterwards.

## Requirements
  * NodeJS
  * npm
  * Python 3
  * LuaLaTeX
  * Production: Nginx (or other webserver)
  * Production: uWSGI (or other application server)
  * Optional: cups

## Dev
```sh
cd backend
pip3 install -r .python-requirements.txt

cd frontend
npm install
```

```sh
cd backend
./server

cd frontend
npm run serve
```
