FROM alpine:3.8 as stm_checklist_base
RUN \
	set -ex && \
	apk add --no-cache nodejs npm python3 texlive-luatex nginx uwsgi-python3 supervisor cups cups-filters && \
	ln -s luatex /usr/bin/lualatex


FROM stm_checklist_base as stm_checklist
COPY . /tmp/stm_checklist
RUN \
	set -ex && \
	cd /tmp/stm_checklist/frontend/ && \
		npm install && \
		npm run build && \
		cp -R /tmp/stm_checklist/frontend/dist/* /var/www/localhost/htdocs/ && \
	cd /tmp/stm_checklist/backend/ && \
		pip3 install -r .python-requirements.txt && \
	cd /tmp/stm_checklist/docker && \
	cp nginx.conf       /etc/nginx/nginx.conf && \
	cp uwsgi.ini        /etc/uwsgi/uwsgi.ini && \
	cp supervisord.conf /etc/supervisord.conf && \
	mkdir -p /opt/stm_checklist && \
	cp -r /tmp/stm_checklist/backend /opt/stm_checklist/backend && \
	rm -R /tmp/stm_checklist

EXPOSE 80 631
VOLUME "/etc/cups"
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
