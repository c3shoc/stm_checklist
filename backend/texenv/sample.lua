module(...,package.seeall)
qrcodelib = dofile("qrencode.lua")

local function black_box(w,h)
	return "\\vrule width "..w.." height "..h
end

local function white_box(w,h)
	return "\\hskip "..w
end

function generate_matrix2(s,w,h)
	local ok, tab_or_message = qrcodelib.qrcode(s)
	local buffer = {}
	local write_nl = function(text)
		table.insert(buffer,text)
	end
	
	if not ok then
		print(tab_or_message)
	else
		write_nl("\\bgroup")
		
		write_nl("\\baselineskip="..h)
		local x = #tab_or_message[1]
		local y = #tab_or_message
		for i = 1, x, 1 do
			local boxes = {}
			for n = 1, y, 1 do
				local p = tab_or_message[n][i] > 0  and black_box(w,h) or white_box(w,h)
				table.insert(boxes,p)
			end
			write_nl("\\hbox{"..table.concat(boxes).."}")
		end
		
		write_nl("\\egroup")
	end
	
	return table.concat(buffer,"")
end
