import flask
import flask_cors
import checklist
import os



app = flask.Flask(__name__)
flask_cors.CORS(app, resources={
	r'/api/*': {'origins': '*', 'expose_headers': ['Content-Disposition']}
})


x = checklist.Checklist()
def ckl():
	return x


## curl  -i  'http://localhost:5000/api/v1/schedule'
## curl  -i  -H 'Content-Type: application/json'  -X POST  -d '{"update": true}'  'http://localhost:5000/api/v1/schedule'
@app.route('/api/v1/schedule', methods=['GET', 'POST'])
def api_v1_schedule():
	if flask.request.json and 'update' in flask.request.json and flask.request.json['update'] == True:
		ckl().updateSchedule()
	
	return flask.Response(ckl().getScheduleJson(), mimetype='application/json')


## curl  -v  -o /tmp/foo.pdf  -H 'Content-Type: application/json'  -X POST  -d '{"ids": [8956, 9113, 8848], "type": "open_pdf"}'  'http://localhost:5000/api/v1/pdf'
@app.route('/api/v1/pdf', methods=['POST'])
def api_v1_pdf():
	if flask.request.json['type'] == 'open_pdf':
		f = ckl().getPdf(flask.request.json['ids'], 'output.pdf')
		response = flask.send_file(f, 'application/pdf')
	
	elif flask.request.json['type'] == 'download_pdf':
		f = ckl().getPdf(flask.request.json['ids'], 'output.pdf')
		response = flask.send_file(f, 'application/pdf', True, 'checklist.pdf')
	
	elif flask.request.json['type'] == 'logs':
		f = ckl().getPdf(flask.request.json['ids'], 'logs.zip')
		response = flask.send_file(f, 'application/zip', True, 'logs.zip')
	
	else:
		return
	
	return response
