import json
import pprint
import subprocess
import re
import os
import math
import util
import requests
import email.utils
import datetime
import hashlib
import shutil
import zipfile
import enum
import tempfile
import worker



def parseTime(s):
	parts = s.split(':')
	return datetime.timedelta(hours=int(parts[0]), minutes=int(parts[1]))


def parseDateTime(s):
	return datetime.datetime.strptime(s[:19], '%Y-%m-%dT%H:%M:%S')



class Checklist:
	def __init__(self):
		self._lastScheduleUpdate = datetime.datetime(1970, 1, 1)
		self._schudulePath = 'schedule.json'
		self._scheduleJson = None
		self._schedule = None
		
		self._tmpDir = tempfile.mkdtemp(prefix='stm_ckl-')
		for item in os.listdir('texenv'):
			s = os.path.join('texenv', item)
			d = os.path.join(self._tmpDir, item)
			if os.path.isfile(s):
				shutil.copy2(s, d)
		
		self._readTemplate()
		self._workers = []
		for i in range(4):
			w = worker.Worker('%s/job%s'%(self._tmpDir, i), self._tplStart, self._tplEnd)
			w.reuse()
			self._workers.append(w)
		
		if not os.path.isfile(self._schudulePath):
			self.updateSchedule()
	
	
	#def __del__(self):
		#for w in self._workers:
			#w.stop()
		
		#shutil.rmtree(self._tmpDir)
	
	
	def updateSchedule(self):
		now = datetime.datetime.now()
		td = (now - self._lastScheduleUpdate).total_seconds()
		if td < 5:
			print('Please be patient updating (%ss)' % td)
			return
		
		r = requests.get('https://events.ccc.de/congress/2017/Fahrplan/schedule.json')
		data = r.json()
		
		## Extend original schedule with caching related infos
		data['cache_last_modified'] = datetime.datetime(*email.utils.parsedate(r.headers['Last-Modified'])[:6]).isoformat()
		data['cache_etag'] = r.headers['ETag']
		#data['cache_last_check'] = datetime.datetime.now().isoformat() ## TODO If we want to comapre and version the updates later, this needs to be saved somewhere else (file metdatale like mdate?)
		
		## Atomicaly replace cached schedule
		tmpPath = 'tmp/download-%s.json'%os.getpid()
		with open(tmpPath, 'x') as f:
			json.dump(data, f, indent=True)
			f.write('\n')
		os.rename(tmpPath, self._schudulePath)
		
		self._scheduleJson = None
		self._schedule = None
		self._lastScheduleUpdate = now
		print('Update sucessful')
	
	
	def getScheduleJson(self):
		if self._scheduleJson is None:
			with open(self._schudulePath, 'r') as f:
				self._scheduleJson = f.read()
		
		return self._scheduleJson
	
	
	def getSchedule(self):
		if self._schedule is None:
			self._schedule = json.loads(self.getScheduleJson())
			self._talks = {}
			
			## Prepare data
			self._schedule['cache_last_modified'] = parseDateTime(self._schedule['cache_last_modified'])
			
			for day in self._schedule['schedule']['conference']['days']:
				for talks in day['rooms'].values():
					for talk in talks:
						talk['day_nr'] = day['index']
						talk['day_date'] = day['date']
						talk['start'] = parseDateTime(talk['date'])
						del talk['date']
						talk['duration'] = parseTime(talk['duration'])
						talk['end'] = talk['start'] + talk['duration']
						self._talks[talk['id']] = talk
			
		return self._schedule
	
	
	def getPdf(self, ids, name):
		h = hashlib.sha256()
		h.update(','.join(map(str, ids)).encode('utf-8'))
		hsh = h.hexdigest()
		
		cacheDir = 'cache/%s'%hsh
		cacheFile = '%s/%s'%(cacheDir, name)
		try:
			f = open(cacheFile, 'rb')
		except FileNotFoundError as ex:
			if not os.path.isdir(cacheDir):
				os.mkdir(cacheDir)
			
			## Distribute workload to workers
			chunkSize = math.ceil(len(ids) / len(self._workers))
			chunks = list(map(lambda x: ids[x:x + chunkSize], range(0, len(ids), chunkSize)))
			
			self.getSchedule()
			for i, work in enumerate(chunks):
				self._workers[i].do(work, self._talks)
			
			## Collect work from workers
			mergeCmd = ['pdfunite']
			with zipfile.ZipFile('%s/logs.zip' % cacheDir, 'w') as archive:
				for i in range(len(chunks)):
					fileBase = self._workers[i].wait()
					archive.write(fileBase+'.log', 'logs/worker_%s.log'%i)
					mergeCmd.append(fileBase+'.pdf')
			
			mergeCmd.append('%s/output.pdf' % cacheDir)
			subprocess.run(mergeCmd, check=True)
			
			## Free workers
			for i in range(len(chunks)):
				self._workers[i].reuse()
			
			## TODO clean cache
			
			## Try again opening the requested file
			f = open(cacheFile, 'rb')
		
		return f
	
	
	def _readTemplate(self):
		self._tplStart = ''
		self._tplEnd = ''
		
		with open('template.tex') as tpl:
			for line in tpl:
				if line != '%%%%%%%%%%%%%%%% BEGIN EXAMPLE\n':
					self._tplStart += line
				else:
					break
			
			for line in tpl:
				if line == '%%%%%%%%%%%%%%%% END EXAMPLE\n':
					break
			
			for line in tpl:
				self._tplEnd += line


#pdfunite
#https://tex.stackexchange.com/questions/397155/generate-a-list-of-qr-code-with-lua
## Normal, in /tmp, in /tmp und >/dev/null
#5: 22  22
#4: 21  21  19
#3: 22  22  20
#2: 28  22  20
#1: 35  35  33

## IDs: [8956, 9113, 8848]