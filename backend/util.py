import re



def replace(text, replacements):
	replacements = dict((re.escape(k), v) for k, v in replacements.items())
	pattern = re.compile("|".join(replacements.keys()))
	return pattern.sub(lambda m: replacements[re.escape(m.group(0))], text)
