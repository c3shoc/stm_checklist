import subprocess
import os
import util
import enum



def _escape(s):
	return util.replace(s, {
		'&': r'\&',
		'%': r'\%',
		'$': r'\$',
		'#': r'\#',
		'_': r'\_',
		'{': r'\{',
		'}': r'\}',
		'~': r'\textasciitilde{}',
		'^': r'\^{}',
		'\\': r'\textbackslash{}',
		'<': r'\textless{}',
		'>': r'\textgreater{}',
	})



class WORKER_STATE(enum.Enum):
	STOPPED = enum.auto()
	PRELOADED = enum.auto()
	RUNNING = enum.auto()
	DONE = enum.auto()



class Worker:
	def __init__(self, fileBase, tplStart, tplEnd):
		self._fileBase = fileBase
		self._tplStart = tplStart
		self._tplEnd   = tplEnd
		self._state = WORKER_STATE.STOPPED
		self._f = None
		self._proc = None
	
	
	#def __del__(self):
		#self.stop()
	
	
	def wait(self):
		if self._state != WORKER_STATE.RUNNING:
			raise Exception('Can\'t reuse() in %s state' % self._state)
		self._state = WORKER_STATE.DONE
		
		os.waitpid(self._proc.pid, 0) ## No busy waiting, compared to Popen.wait()
		return self._fileBase
	
	
	def reuse(self):
		if self._state != WORKER_STATE.STOPPED and self._state != WORKER_STATE.DONE:
			raise Exception('Can\'t reuse() in %s state' % self._state)
		self._state = WORKER_STATE.PRELOADED
		
		texFile = self._fileBase+'.tex'
		if os.path.exists(texFile):
			os.remove(texFile)
		os.mkfifo(texFile)
		
		self._proc = subprocess.Popen(
			['lualatex', '--interaction=batchmode', '--halt-on-error', '--no-shell-escape', '--nosocket', os.path.basename(texFile)],
			cwd=os.path.dirname(texFile),
			stdout=subprocess.DEVNULL,
			stdin=subprocess.DEVNULL
		)
		
		self._f = open(texFile, 'w')
		self._f.write(self._tplStart)
	
	
	def do(self, ids, talks):
		if self._state != WORKER_STATE.PRELOADED:
			raise Exception('Can\'t reuse() in %s state' % self._state)
		self._state = WORKER_STATE.RUNNING
		
		for talk in ids:
			talk = talks[talk]
			self._f.write('\t\n')
			
			md = {
				'Version':   '2018-10-09 12:34',
				'Hall':      talk['room'],
				'DayNr':     talk['day_nr'],
				'DayDate':   talk['day_date'][0:10],
				'StartDate': talk['start'].strftime('%Y-%m-d'),
				'StartTime': talk['start'].strftime('%H:%M'),
				'EndDate':   talk['end'].strftime('%Y-%m-d'),
				'EndTime':   talk['end'].strftime('%H:%M'),
				'Title':     talk['title'],
				'Subtitle':  talk['subtitle'],
				'Track':     talk['track'],
				'Type':      talk['type'],
				'Language':  talk['language'],
				'Speakers':  ', '.join(map(lambda x: x['public_name'], talk['persons'])),
				'Url':       talk['url'],
			}
			
			for k, v in md.items():
				self._f.write('\t\\def \md%s {%s}\n' % (k, _escape(str(v))))
			
			if talk['do_not_record']:
				self._f.write('\t\\doNotRecordtrue\n')
			else:
				self._f.write('\t\\doNotRecordfalse\n')
			
			self._f.write('\t\\handout\n')
		
		self._f.write(self._tplEnd)
		self._f.close()
		self._f = None
	
	
	def stop(self):
		if self._f is not None:
			self._f.close()
			self._f = None
		
		if self._proc is not None:
			self._proc.terminate()
			self._proc = None
		
		self._state = WORKER_STATE.STOPPED
